<?php
/**
 * Created by PhpStorm.
 * User: tazen
 * Date: 15/01/19
 * Time: 16:27
 */

namespace lbs\store\models;

class Command extends \Illuminate\Database\Eloquent\Model{

    protected $table = 'commande';
    protected $primaryKey = 'id';
    protected $timestamp = true;
    public $incrementing = false;
    protected $keyType = 'string';

    public function items(){
        return $this->hasMany(
            'lbs\store\models\Item',
            'command_id');
    }


}
