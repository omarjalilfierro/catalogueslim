<?php
/**
 * Created by PhpStorm.
 * User: tazen
 * Date: 15/01/19
 * Time: 16:32
 */

namespace lbs\store\models;

class Item extends \Illuminate\Database\Eloquent\Model{

    protected $table = 'item';
    protected $primaryKey = 'id';
    protected $timestamp = false;

    public function command(){
        return $this->belongsTo(
            'lbs\store\models\Command',
            'command_id');
    }


}
