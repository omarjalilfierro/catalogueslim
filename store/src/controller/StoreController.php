<?php
/**
 * Created by PhpStorm.
 * User: jalil
 * Date: 12/13/18
 * Time: 9:32 AM
 */

namespace lbs\store\controller;

use Exception;
use lbs\store\controller\AbstractController;
use lbs\store\errors\MissingDataException;
//use mysql_xdevapi\Exception;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Container;
use lbs\store\models\Command;
use lbs\store\models\Item;
use lbs\store\response\Writter;
/* Errors */
use lbs\store\errors\NotAllowed;
use lbs\store\errors\NotFound;
use lbs\store\errors\PhpError;
use Slim\Exception\MethodNotAllowedException;

class StoreController{
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /** Gets command by Id
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return Response
     */
    public function getCommand(Request $req, Response $resp, array $args): Response
    {
        try {
            //We can also use Command::findOrFail($args['id'])
            $idCommand = filter_var($args['id'], FILTER_SANITIZE_STRING);

            $command = Command::where('id', $idCommand)->firstOrFail();

            $items=[];
            foreach($command->items()->get() as $item){
                array_push($items, [
                    'item' => $item,
                    'links' => [
                        'self' => [
                            'href' =>  $this->container['router']
                                ->pathFor('itemById',['id'=> $item->id])
                        ]
                    ]
                ]);
            }
            $command['items'] = $items;

            $data = [
                'type' => 'resource',
                'locale' => 'fr_FR',
                'commande' => $command,
                'links' => [
                    'self' => [
                        'href' =>  $this->container['router']
                            ->pathFor('commandById',['id'=> $args['id']])
                        ]
                ]
            ];
            return Writter::jsonSuccess($resp, $data, 200);

        } catch (\Exception $exception) {
            return NotFound::error($req, $resp);
        }
    }


    /** Méthode getItem
     * @param Request $req
     * @param Response $resp
     * @param Array $args
     * @return Response
      */
    public function getItem(Request $req, Response $resp, array $args): Response
    {
        try {
            $idItem = filter_var($args['id'], FILTER_SANITIZE_STRING);

            $item = Item::where('id',$idItem)->firstOrFail();

            $data = [
                'type' => 'resource',
                'locale' => 'fr_FR',
                'item' => $item];
            return Writter::jsonSuccess($resp, $data, 200);

        } catch (\Exception $exception) {
            return NotFound::error($req, $resp);
        }
    }


    /** Méthode getCommandes
     * @param Request $req
     * @param Response $resp
     * @param Array $args
     * @return Response
      */
    public function getCommandes(Request $req, Response $resp, array $args): Response
    {
        try {

            $listeCommandes = Command::all();

            return $this->getAllCommandes($listeCommandes, $resp);

        } catch (\Exception $exception) {
            return NotFound::error($req, $resp);
        }
    }


    /** Méthode getCommandesFilteredByStatus
     * @param Request $req
     * @param Response $resp
     * @param Array $args
     * @return Response
      */
    public function getCommandesFilteredByStatus(Request $req, Response $resp, array $args): Response
    {
        try {
            $statusSearch = $req->getQueryParam('status');

            $listeCommandes = Command::where("status", '=', $statusSearch)->get();

            $filter = "?status=" .$statusSearch;

            return $this->getAllCommandes($listeCommandes, $resp, $filter);

        } catch (\Exception $exception) {
            return NotFound::error($req, $resp);
        }
    }

    /** Méthode getCommandesFilteredByDeliveryDate
     * @param Request $req
     * @param Response $resp
     * @param Array $args
     * @return Response
      */
    public function getCommandesFilteredByDeliveryDate(Request $req, Response $resp, array $args): Response
    {
        try {
            $ascOrDescSearch = $req->getQueryParam('livraison');

            try{
                if ($ascOrDescSearch == 'asc' || $ascOrDescSearch == 'desc' ){
                    $listeCommandes = Command::select()->orderBy('livraison', $ascOrDescSearch)->get();

                    $filter = "?livraison=" .$ascOrDescSearch;

                    return $this->getAllCommandes($listeCommandes, $resp, $filter);
                } else {
                    throw new \Exception (
                        "La livraison doit être ascendante (?livraison=asc) ou descendante (?livraison=desc)"
                        , 500
                    );
                }
            } catch(\Exception $exception){
                return PhpError::error($req, $resp, $exception->getMessage());
            }


        } catch (\Exception $exception) {
            return NotFound::error($req, $resp);
        }
    }


    /** Méthode getCommandesFilteredByCreationDate
     * @param Request $req
     * @param Response $resp
     * @param Array $args
     * @return Response
      */
    public function getCommandesFilteredByCreationDate(Request $req, Response $resp, array $args): Response
    {
        try {
            $ascOrDescSearch = $req->getQueryParam('createdat');

            try{
                if ($ascOrDescSearch == 'asc' || $ascOrDescSearch == 'desc' ){
                    $listeCommandes = Command::select()->orderBy('created_at', $ascOrDescSearch)->get();

                    $filter = "?createdat=" .$ascOrDescSearch;

                    return $this->getAllCommandes($listeCommandes, $resp, $filter);
                } else {
                    throw new \Exception (
                        "La date de création doit être ascendante (?createdat=asc) ou descendante (?createdat=desc)"
                        , 500
                    );
                }
            } catch(\Exception $exception){
                return PhpError::error($req, $resp, $exception->getMessage());
            }


        } catch (\Exception $exception) {
            return NotFound::error($req, $resp);
        }
    }

    /** Méthode getCommandesFilteredByPage
     * @param Request $req
     * @param Response $resp
     * @param Array $args
     * @return Response
      */
    public function getCommandesFilteredByPage(Request $req, Response $resp, array $args): Response
    {
        try {
            $pageNbrSearch = $req->getQueryParam('page');
            $limitSearch = $req->getQueryParam('limit');
            $offset = (--$pageNbrSearch) * $limitSearch;
            $count = Command::all()->count();
            $lastPage = ceil($count / $limitSearch);

            $filter = "?page=" .$pageNbrSearch ."&limit=" .$limitSearch;

            $listeCommandes = Command::skip($offset)->take($limitSearch)->get();

            $commandes = [];

            foreach($listeCommandes as $command){
                array_push($commandes, [
                    'command' => $command,
                    'links' => [
                        'self' => [
                            'href' =>  $this->container['router']
                                ->pathFor('commandById',['id'=> $command->id])
                        ]
                    ]
                ]);
                $items=[];
                foreach($command->items()->get() as $item){
                    array_push($items, [
                        'item' => $item,
                        'links' => [
                            'self' => [
                                'href' =>  $this->container['router']
                                    ->pathFor('itemById',['id'=> $item->id])
                            ]
                        ]
                    ]);
                }
                $command['items'] = $items;
            }

            $data = [
                'type' => 'resource',
                'locale' => 'fr_FR',
                'commandes' => $commandes,
                'links' => [
                    'self' => [
                        'href' =>  $this->container['router']
                                ->pathFor('allCommandes') .$filter
                    ],
                    'prev' => ['href' => $this->container['router']
                        ->pathFor('allCommandes')
                        .'?page=' . ($pageNbrSearch)
                        . '&limit=' . $limitSearch],
                    'next' => ['href' =>  $this->container['router']
                        ->pathFor('allCommandes')
                        .'?page=' . ($pageNbrSearch + 2)
                        . '&limit=' . $limitSearch],
                    'first' => ['href' => $this->container['router']
                        ->pathFor('allCommandes')
                        .'?page=1&limit=' . $limitSearch],
                    'last' => ['href' =>  $this->container['router']
                        ->pathFor('allCommandes')
                        . '?page=' . $lastPage
                        . '&limit=' . $limitSearch]
                ]
            ];

            return Writter::jsonSuccess($resp, $data, 200);

            } catch (\Exception $exception) {
                return NotFound::error($req, $resp);
            }
    }

    /** Méthode getAllCommandes
     * @param Request $req
     * @param Response $resp
     * @param Array $args
     * @return Response
      */
    private function getAllCommandes($listeCommandes, $resp, $filter=null){
        $commandes = [];

        foreach($listeCommandes as $command){
            array_push($commandes, [
                'command' => $command,
                'links' => [
                    'self' => [
                        'href' =>  $this->container['router']
                            ->pathFor('commandById',['id'=> $command->id])
                    ]
                ]
            ]);
            $items=[];
            foreach($command->items()->get() as $item){
                array_push($items, [
                    'item' => $item,
                    'links' => [
                        'self' => [
                            'href' =>  $this->container['router']
                                ->pathFor('itemById',['id'=> $item->id])
                        ]
                    ]
                ]);
            }
            $command['items'] = $items;
        }

        $data = [
            'type' => 'resource',
            'locale' => 'fr_FR',
            'commandes' => $commandes,
            'links' => [
                'self' => [
                    'href' =>  $this->container['router']
                            ->pathFor('allCommandes') .$filter
                ]
            ]
        ];
        return Writter::jsonSuccess($resp, $data, 200);
    }

    /**
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return Response
     */

    public function updateCommandStatus(Request $req, Response $resp, array $args): Response {
        $data = $req->getParsedBody();
        $id = $args['id'];
        try{
            if (empty($data['status'])){
                echo("debug");
                throw new Exception("Etat requis", 405);
            }
            else{
                $command = Command::findOrFail($id);
                $oldStatus = $command->status;
                if (($data['status'] - $oldStatus ==1) && ($data['status'] <= 5) && ($data['status'] > 0)) {
                    $command->status = $data['status'];
                    $command->save();
                } else {
                    return Writter::jsonError($resp, 'Passage à cet état interdit' , 403);
                }
                return Writter::jsonSuccess($resp, $command, 200);
            }
        }catch (\Exception $e){
            return PhpError::error($req, $resp, $e->getMessage());
        }

    }

}




















