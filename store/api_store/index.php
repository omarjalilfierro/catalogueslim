<?php

namespace src\api_store;

use lbs\store\controller\StoreController;
use lbs\store\eloquent\Eloquent;
use lbs\store\errors\MissingDataException;
use lbs\store\response\Writter;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;
use Slim\Container;
use Slim\Exception\MethodNotAllowedException;
use Slim\Exception\NotFoundException;
use Illuminate\Database\Eloquent\ModelNotFoundException;


require '../src/vendor/autoload.php';

$apiContainer = new Container(require_once __DIR__ . "/../src/conf/config.php");
$app = new App($apiContainer);

Eloquent::startEloquent($apiContainer->settings['dbconf']);

$app->get('/commandes/{id}[/]',
    function (Request $req, Response $resp, array $args) {
        return (new StoreController($this))->getCommand($req, $resp, $args);
    }
)->setName('commandById');


$app->get('/items/{id}[/]',
    function (Request $req, Response $resp, array $args) {
        return (new StoreController($this))->getItem($req, $resp, $args);
    }
)->setName('itemById');



$app->get('/commandes[/]',
    function (Request $req, Response $resp, array $args) {
        if ($req->getQueryParam('status')) {
            return (new StoreController($this))->getCommandesFilteredByStatus($req, $resp, $args);
        } elseif ($req->getQueryParam('livraison')) {
            return (new StoreController($this))->getCommandesFilteredByDeliveryDate($req, $resp, $args);
        } elseif ($req->getQueryParam('createdat')) {
            return (new StoreController($this))->getCommandesFilteredByCreationDate($req, $resp, $args);
        } elseif ($req->getQueryParam('page') && $req->getQueryParam('limit')) {
            return (new StoreController($this))->getCommandesFilteredByPage($req, $resp, $args);
        } else {
            return (new StoreController($this))->getCommandes($req, $resp, $args);
        }
    }
)->setName('allCommandes');

$app->put('/commandes/{id}',
    function (Request $req, Response $resp, array $args) {
        return (new StoreController($this))->updateCommandStatus($req, $resp, $args);
    }
)->setName('updateCommandStatus');

$app->run();