<?php

use lbs\command\controller\CommandAuthController;
use lbs\command\controller\CommandController;
use lbs\command\eloquent\Eloquent;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;
use Slim\Container;
use lbs\command\models\Command;
use lbs\command\errors\NotFound;
use lbs\command\response\Writter;

require '../src/vendor/autoload.php';

$apiContainer = new Container(require_once __DIR__ . "/../src/conf/config.php");
$app = new App($apiContainer);

$verifToken = function (Request $req,Response $resp, $next) {
    $query = $req->getQueryParams();
    if(isset($query['token'])){
        $token = filter_var($query['token'], FILTER_SANITIZE_STRING);

        $result = Command::where('token','=',$token)->first();

        if(!empty($result)){
            $response = $next($req, $resp);
        }
        else{
            return NotFound::error($req, $resp);
        }
    }
    else{
        return Writter::jsonError($resp, "Missing token", 403);
    }

    return $response;
};

Eloquent::startEloquent($apiContainer->settings['dbconf']);

$app->post('/commandes[/]',
    function (Request $req, Response $resp, array $args){
        return (new CommandController($this))->createCommand($req, $resp, $args);
    }
);

$app->put('/commandes/{id}',
    function(Request $req, Response $resp, array $args){
        try {
            return (new CommandController($this))->updateDate($req, $resp, $args);
        }catch (\Exception $e){
            echo $e->getMessage();
        }
    }
);

$app->get('/commandes/{id}[/]',
    function (Request $req, Response $resp, array $args){
        try {
            return (new CommandController($this))->getCommand($req, $resp, $args);
        } catch (Exception $e) {
        }
    })->setName('commandes');

$app->get('/test',
    function (Request $req, Response $resp, array $args){
        try {
            return (new CommandController($this))->getCommands($req, $resp, $args);
        } catch (Exception $e) {
        }
    })->setName('test');

/*$app->get('/clients/{id}/auth',
    function (Request $req, Response $resp, array $args){
        try {
            return (new CommandAuthController($this))->login($req, $resp, $args);
        } catch (Exception $e) {
        }
    })->setName('auth');*/

$app->post('/clients/auth',
    function (Request $req, Response $resp, array $args){
        try {
            return (new CommandAuthController($this))->login($req, $resp, $args);
        } catch (Exception $e) {
        }
    })->setName('auth');

$app->get('/clients/{id}',
    function (Request $req, Response $resp, array $args){
        try {
            return (new CommandAuthController($this))->checkToken($req, $resp, $args);
        } catch (Exception $e) {
        }
    })->setName('client');

$app->post('/clients[/]',
    function(Request $request, Response $response, array $args){
        return (new CommandAuthController($this))->createUser($request,$response, $args);
    })->setName('auth');

$app->get('/commandes/{id}/facture[/]',
    function (Request $req, Response $resp, array $args){
        return (new CommandController($this))->getFacture($req, $resp, $args);
    })->add($verifToken);

$app->post('/commandes/{id}/pay',
    function (Request $req, Response $resp, array $args){
        return (new CommandController($this))->payCommand($req, $resp, $args);
    });

$app->get('/clients/{id}/commandes[/]',
    function (Request $req, Response $resp, array $args){
        return (new CommandController($this))->getHistory($req, $resp, $args);
    });
$app->run();