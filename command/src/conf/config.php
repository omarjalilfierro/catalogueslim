<?php
/**
 * Created by PhpStorm.
 * User: jalil
 * Date: 12/13/18
 * Time: 10:11 AM
 */

return [
    'settings' => ['displayErrorDetails' => true,
                   'dbconf' => __DIR__ . '/config.ini'],
    //Slim named this error 'notFoundHandler','notAllowedHandler'
    //and it matches them automatically
    'notFoundHandler' => function ($container){
        return function ($rq, $rs){
            return \lbs\command\errors\NotFound::error($rq, $rs);
        };
    },
    'notAllowedHandler' => function ($container){
        return function ($rq, $rs,$methods){
            return \lbs\command\errors\NotAllowed::error($rq,$rs,$methods);
        };
    },
    'phpErrorHandler' => function ($container){
        return function ($rq, $rs,$error){
            return \lbs\command\errors\PhpError::error($rq,$rs,$error);
        };
    }


];
