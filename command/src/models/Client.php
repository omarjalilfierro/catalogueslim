<?php
/**
 * Created by PhpStorm.
 * User: jalil
 * Date: 2/12/19
 * Time: 6:11 PM
 */

namespace lbs\command\models;


use Illuminate\Database\Eloquent\Model;

class Client extends Model{

    protected $table = 'client';
    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'string';

    public function commands(){
        return $this->hasMany('lbs\command\models\Command','client_id');
    }
}