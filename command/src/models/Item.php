<?php
/**
 * Created by PhpStorm.
 * User: tazen
 * Date: 15/01/19
 * Time: 16:32
 */
namespace lbs\command\models;

class Item extends \Illuminate\Database\Eloquent\Model{

    protected $table = 'item';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function command(){
        return $this->belongsTo(
            'lbs\command\models\Command',
            'command_id');
    }


}
