<?php
namespace lbs\command\utils;

use Exception;
use Firebase\JWT\BeforeValidException;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT;
use Firebase\JWT\SignatureInvalidException;
use Psr\Http\Message\ServerRequestInterface as Request;

class Utils {

    /**
     * recupère et décode un JWT si présent dans le header la requete
     * renvoi le token décodé
     *
     * @param Request $req
     *
     * @throws Exception
     * @return object
     *
     */
    public static function getTokenData(Request $req){
        $token = false;
        $aut = $req->getHeader('Authorization');
        if (is_array($req->getHeader('Authorization')) && isset($req->getHeader('Authorization')[0])) {
            $header = $req->getHeader('Authorization')[0];
            $tokenString = sscanf($header, "Bearer %s")[0];
            /*TODO sauvegarder la key dans un config file*/
            $token = JWT::decode($tokenString, '12345', ['HS512']);
        }
        return  $token;

    }

}