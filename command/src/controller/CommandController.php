<?php
namespace lbs\command\controller;

use Exception;
use Firebase\JWT\JWT;
use lbs\command\models\Client;
use lbs\command\models\Command;
use lbs\command\models\Item;
use lbs\command\utils\Utils;
use Slim\Container;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use lbs\command\response\Writter;
use Ramsey\Uuid\Uuid;
use Firebase\JWT\BeforeValidException;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\SignatureInvalidException;
/* Errors */
use lbs\command\errors\NotAllowed;
use lbs\command\errors\NotFound;
use lbs\command\errors\PhpError;


class CommandController{
    private $container;     

    public function __construct(Container $container){
        $this->container = $container;
    }

    /** POST command
     * @param Request $req
     * @param Response $resp
     * @param array $args
     * @return mixed
      */
    public function createCommand(Request $req, Response $resp, array $args): Response{

        $tokenData = false;
        try {

            $tokenData = Utils::getTokenData($req);

        } catch (\Exception $e) {
            // si le token n'est pas bon on laisse en non identifié
        }

        $body = $req->getParsedBody();

        $error = false;
        if($tokenData === false) {
            // pas connecté => on recupere les infos dans la requete
            $clientId = null;
            if (filter_var($body['nom'], FILTER_SANITIZE_STRING)) {
                $nom = filter_var($body['nom'], FILTER_SANITIZE_STRING);
            } else {
                $error = true;
            }
            if (filter_var($body['mail'], FILTER_VALIDATE_EMAIL)) {
                $mail = filter_var($body['mail'], FILTER_VALIDATE_EMAIL);
            } else {
                $error = true;
            }
        } else {
            // connecté => recup des infos du client en bdd
            try {
                $client = Client::findOrFail($tokenData->uid);
                $clientId = $client->id;
                // on recupere le nom et l'email du client
                // est-ce que c'est n&cessaire vu que avec le client_id on peut le retrouver
                $nom = $client->nom;
                $mail = $client->mail;

            } catch (\Exception $e) {
                return \lbs\catalogue\errors\NotAllowed::error($req,$resp,['clients/auth']);
            }
        }


        if(isset($body['livraison']['date'])){
            $date['date'] = filter_var($body['livraison']['date'], FILTER_SANITIZE_STRING);
        }
        else{
            $error = true;
        }
        if(filter_var($body['livraison']['heure'], FILTER_SANITIZE_STRING)){
            $date['heure'] = filter_var($body['livraison']['heure'], FILTER_SANITIZE_STRING);
        }
        else{
            $error = true;
        }
        if(!isset($date['date']) || !isset($date['heure'])){
            $error = true;
        }
        else{
            $datetime = $date['date']." ".$date['heure'];
            $livraison = new \DateTime($datetime);
        }

        if(!$error){

            try{
                
                do {
                    $token = random_bytes(32);
                    $token = bin2hex($token);
                    $tokenCommand = Command::where('token','=',$token)->first(); 
                } while (!empty($tokenCommand));

                do {
                    $uuid = Uuid::uuid4()->toString();
                    $uuidCommand = Command::where('id','=',$uuid)->first();
                } while (!empty($uuidCommand));

                $command = new Command();
                $command->id = $uuid;
                $command->nom = $nom;
                $command->mail = $mail;
                $command->client_id = $clientId;
                $command->livraison = $livraison->format('Y-m-d H:i:s');
                $command->token = $token;
                $command->montant = 0;
                $command->status = 1;
                $command->save();

                if(isset($body['items'])){
                    $clientHttp = new \GuzzleHttp\Client(['base_uri' => 'http://api.catalogue-demo.local']);

                    $totalPrice = 0;
                    foreach ($body['items'] as $item) {
                        
                        try{
                            $response = $clientHttp->request('GET', $item['uri']);
                            $postData = json_decode($response->getBody()->getContents());

                            $newItem = new Item();
                            $newItem->uri = $item['uri'];
                            $newItem->libelle = $postData->sandwich[0]->nom;
                            $newItem->tarif = $postData->sandwich[0]->price;
                            $newItem->quantite = $item['q'];
                            $newItem->command_id = $command->id;
                            $newItem->save();

                            $totalPrice += $newItem->quantite * $newItem->tarif;
                        }
                        catch (\Exception $exception) {
                            return PhpError::error($req, $resp, $exception->getMessage());
                        }
                    }
                    $command->items;
                    $command->montant = $totalPrice;
                    $command->save();
                }
                /*unset($command->created_at);
                unset($command->updated_at);
                unset($command->livraison);
                $command->livraison = ['date' => $date['date'], 'heure' => $date['heure']];
                $data['commande'] = $command;
                */
                $resp = Writter::jsonSuccess($resp, ['token'=>$command->token], 201);

                return $resp->withHeader('Location', $this->container['router']->pathFor('commandes',['id'=> $command->id]));
    
            }
            catch (\Exception $exception) {
                return PhpError::error($req, $resp, $exception->getMessage());
            }
        }
        else{
            return Writter::jsonError($resp, "Missing data", 403);
        }
    }

    /** Update "livraison" in Commands
     * @param Request $req
     * @param Response $resp
     * @param array $args               'date_livr'
     * @return Response
     */
    public function updateDate(Request $req, Response $resp, array $args): Response{
        $data = $req->getParsedBody();
        $id = $args['id'];
        try{
            if (empty($data['date_livr']))
                throw new Exception("Livraison requis", 405);
            else{
                $command = Command::findOrFail($id);
                $command->livraison = $data['date_livr'];
                $command->save();

                return Writter::jsonSuccess($resp, $command, 200);
            }
        }catch (\Exception $e){
            return PhpError::error($req, $resp, $e->getMessage());
        }
    }

    /** Gets command by Id
     * @param Request $req
     * @param Response $resp
     * @param array $args           'id'
     * @return mixed
     */
    public function getCommand(Request $req, Response $resp, array $args){

        try {
            $command = Command::findOrFail($args['id']);
            /* $commandDetails = [
                'self' => [
                    'href' => '/commands/' . $args['id']
                ]
            ]; */
            $data = [
                'type' => 'resource',
                'locale' => 'fr_FR',
                'category' => $command/* ,
                'links' => $commandDetails */];
            return Writter::jsonSuccess($resp, $data, 200);
        } catch (Exception $e) {
            return NotFound::error($req, $resp);
        }

    }

    /** Méthode getCommands
     * @param Request $req
     * @param Response $resp
     * @param Array $args
     * @return Response
      */
    public function getCommands(Request $req, Response $resp, array $args){
        $commands = Command::all();
        return Writter::jsonSuccess($resp, $commands, 200);
    }
    
    /** Méthode getFacture
     * @param Request $req
     * @param Response $resp
     * @param Array $args
     * @return Response
      */
    public function getFacture(Request $req, Response $resp, array $args){
        $id = filter_var($args['id']);
        $query = $req->getQueryParams();
        $command = Command::where('id','=',$id)->where('token','=',$query['token'])->where('status','=',2)->first();

        if(!empty($command)){
            $command->items;
            $data['commande'] = $command;
            return Writter::jsonSuccess($resp, $data, 200);
        }
        else{
            return NotFound::error($req, $resp);
        }
    }

    /** Méthode payCommand
     * @param Request $req
     * @param Response $resp
     * @param Array $args
     * @return Response
      */
    public function payCommand(Request $req, Response $resp, array $args){
        $data = $req->getParsedBody();
        $money = $data['money'];
        $noCB = $data['cb'];
        $cvv = $data['cvv'];
        $exp = $data['exp'];
        $wantToUseCumul = $data['wantToUseCumul'];

        $commandId = $args['id'];

        $header = $req->getHeader('Authorization')[0];
        $tokenString = sscanf($header, "Bearer %s")[0];
        $token = JWT::decode($tokenString, '12345', ['HS512']);

        $clientId = $token->uid;
        $client = Client::find($clientId)->first();
        $cumul = $client->cumul;

        if ($wantToUseCumul){
            $money -= $cumul; //Money - cagnote
            $client->cumul = 0.0;
            $client->save();
            if (isset($commandId)){
                $command = Command::find($commandId)->first();
                $command->remise = $cumul;
                $command->montant = $money;
                $command->save();
            return Writter::jsonSuccess($resp,$command,200);
            }
        }else {
            $client->cumul += $cumul * 0.05; //cagnote++
            $client->save();
            if (isset($commandId)){
                $command = Command::find($commandId)->first();
                $command->remise = 0.0;
                $command->montant = $money;
                $command->save();
                return Writter::jsonSuccess($resp,$command,200);
            }
        }

        return Writter::jsonError($resp, 'asd',400);
    }

    /** Méthode decodeJWT
     * @param Request $request
     * @param Response $resp
     * @return Response ERROR or $token
      */
    static public function decodeJWT($request, $resp){
        $header = $request->getHeader('Authorization')[0];
        $tokenString = sscanf($header, "Bearer %s")[0];
        $token = JWT::decode($tokenString, '12345', ['HS512']);
        if (empty($token)){
            return Writter::jsonError($resp, 'Empty Token', 400);
        }else
            return $token;

    }

    /** Méthode getHistory
     * @param Request $req
     * @param Response $resp
     * @param Array $args
     * @return Response
      */
    public function getHistory(Request $req, Response $resp, array $args){
        if(filter_var($args['id'], FILTER_SANITIZE_STRING)){
            $id = filter_var($args['id'], FILTER_SANITIZE_STRING);

            $client = Client::find($id);

            if(!empty($client)){
                return Writter::jsonSuccess($resp, array("commandes" => $client->commands), 200);
            }
            else{
                return NotFound::error($req, $resp);
            }
        }
        else{
            return Writter::jsonError($resp, "Missing data", 403);
        }
    }
}