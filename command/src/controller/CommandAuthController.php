<?php
/**
 * Created by PhpStorm.
 * User: jalil
 * Date: 2/12/19
 * Time: 4:45 PM
 */
namespace lbs\command\controller;

use Firebase\JWT\BeforeValidException;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT;
use Firebase\JWT\SignatureInvalidException;
use lbs\command\middleware\JwtAuth;
use lbs\command\models\Client;
use lbs\command\response\Writter;
use Ramsey\Uuid\Uuid;
use Slim\Container;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;


class CommandAuthController extends JwtAuth{

    private $container;
    protected $privateKey;

    public function __construct(Container $container){
        $this->container = $container;
    }

    /** Méthode checkToken
     * @param Request $req
     * @param Response $resp
     * @param Array $args
     * @return Response
      */
    public function checkToken(Request $request, Response $response, array $args){
        $header = $request->getHeader('Authorization')[0];
        $tokenString = sscanf($header, "Bearer %s")[0];
        try{
                                                /*TODO sauvegarder la key dans un config file*/
            $token = JWT::decode($tokenString, '12345', ['HS512']);
            return Writter::jsonSuccess($response, array ("token" => $token), 200);
        }catch (ExpiredException $exception){
            return Writter::jsonError($response, $exception, 400);
        }catch (SignatureInvalidException $exception){
            return Writter::jsonError($response, $exception, 400);
        }catch (BeforeValidException $exception){
            return Writter::jsonError($response, $exception, 400);
        }catch (\UnexpectedValueException $exception){
            return Writter::jsonError($response, $exception, 400);
        }

//
//        return Writter::jsonSuccess($response, array("token" => $token),200);

    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return mixed
     */
    public function login(Request $request, Response $response, array $args)
    {

        // si pas de login ou pas de mdp -> erreur
        if (!isset($_SERVER['PHP_AUTH_USER']) || !isset($_SERVER['PHP_AUTH_PW'])) {
            return Writter::jsonError($response, 'Credentials required', 401);
        }

        $email = $_SERVER['PHP_AUTH_USER'];
        $pass = $_SERVER['PHP_AUTH_PW'];

        try {
            $client = Client::where('mail', '=', $email)->first();
        } catch (\Exception $exception) {
            $client = false;
        }
        if ($client && password_verify($pass, $client->mdp)) {
            $token = [
                "iss" => "http://api.command-demo.local",
                "aud" => "http://api.command-demo.local", //audience
                "uid" => $client->id,
                "iat" => time(),
                'exp' => time() + 36000,
            ];
            /*TODO sauvegarder la key dans un config file*/
            $jwt = JWT::encode($token, '12345', 'HS512');
            $data = [
                "Token" => $jwt
            ];
            return Writter::jsonSuccess($response, $data, 200);
            /*https://tools.ietf.org/html/rfc7519*/
        } else {
            return Writter::jsonError($response, 'Bad Credentials', 401);
        }
    }

    /** Méthode createUser
     * @param Request $req
     * @param Response $resp
     * @param Array $args
     * @return Response
      */
    public function createUser(Request $req, Response $resp, array $args): Response{
        //creates user
        $data = $req->getParsedBody();

        $client = new Client();
        $uuid = Uuid::uuid4()->toString();
        $client->id = $uuid;
        $client->nom = $data['nom'];
        $client->mail = $data['mail'];
        $client->mdp = password_hash($data['mdp'],PASSWORD_DEFAULT);
        $client->save();

        return Writter::jsonSuccess($resp, $client, 200);

    }



}