<?php
/**
 * Created by PhpStorm.
 * User: jalil
 * Date: 12/13/18
 * Time: 9:32 AM
 */

namespace lbs\catalogue\controller;

use lbs\catalogue\errors\NotFound;
use lbs\catalogue\errors\PhpError;
use lbs\catalogue\models\Categorie;
use lbs\catalogue\models\Sandwich;
use lbs\catalogue\response\Writter;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Container;

/* Errors */


class CatalogueController
{

    protected $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /** Méthode GET categories
     * @param Request $req
     * @param Response $resp
     * @param Array $args
     * @return Response
     */
    public function getCategories(Request $req, Response $resp, array $args): Response
    {
        try {
            $categories = [];
            $listCategories = Categorie::all(['id', 'nom']);
            foreach ($listCategories as $listCategory) {
                array_push($categories, [
                    'category' => $listCategory,
                    'links' => ['self' => ['href' => $this->container['router']
                        ->pathFor('categoryById', ['id' => $listCategory->id])
                    ]]
                ]);
            }
            $data = [
                'count' => count($categories),
                'categories' => $categories];
            return Writter::jsonSuccess($resp, $data, 200, 'collection');
        } catch (\Exception $exception) {
            return PhpError::error($req,$resp, $exception->getMessage());
        }
    }

    /** Méthode GET category by id
     * @param Request $req
     * @param Response $resp
     * @param Array $args
     * @return Response
     */
    public function getCategory(Request $req, Response $resp, array $args): Response
    {
        try {
            $category = Categorie::where('id', $args['id'])->firstOrFail();
            $categoryDetails = [
                'self' => [
                    'href' => $this->container['router']
                        ->pathFor('categoryById', ['id' => $args['id']])
                ],
                'sandwiches' => [
                    'href' => $this->container['router']
                        ->pathFor('sandwichesOfACategory', ['id' => $args['id']])
                ]
            ];
            $data = [
                'category' => $category,
                'links' => $categoryDetails];
            return Writter::jsonSuccess($resp, $data);

        } catch (\Exception $exception) {
            return NotFound::error($req, $resp);
        }
    }

    /** Méthode GET Sandwiches By category
     * @param Request $req
     * @param Response $resp
     * @param Array $args
     * @return Response
     */
    public function getSandwichesByCategory(Request $req, Response $resp, array $args): Response
    {
        try {
            $category = [];
            $theCategory = Categorie::where('id', $args['id'])->with('sandwiches')->get();
            foreach ($theCategory as $cat) {
                $sandwiches = [];
                foreach ($cat->sandwiches as $sandwich) {
                    array_push($sandwiches, [
                        'sandwich' => [
                            'id' => $sandwich->id,
                            'nom' => $sandwich->nom
                        ],
                        'links' => [
                            'self' => [
                                'href' => $this->container['router']
                                    ->pathFor('sandwichById', ['id' => $sandwich->id])
                            ]
                        ]
                    ]);
                }
                array_push($category, [
                    'id' => $cat->id,
                    'nom' => $cat->nom,
                    'description' => $cat->description,
                    'sandwiches' => $sandwiches
                ]);
            }
            $categoryDetails = [
                'self' => [
                    'href' => $this->container['router']
                        ->pathFor('categoryById', ['id' => $args['id']])
                ],
                'sandwiches' => [
                    'href' => $this->container['router']
                        ->pathFor('sandwichesOfACategory', ['id' => $args['id']])
                ]
            ];
            $data = [
                'category' => $category,
                'links' => $categoryDetails];
            return Writter::jsonSuccess($resp, $data);

        } catch (\Exception $exception) {
            return NotFound::error($req, $resp);
        }
    }

    /** Méthode GET Sandwiches
     * @param Request $req
     * @param Response $resp
     * @param Array $args
     * @return Response
     */
    public function getSandwiches(Request $req, Response $resp, array $args): Response
    {
        try {
            $sandwiches = [];
            $listSandwiches = Sandwich::all(['id', 'nom']);
            foreach ($listSandwiches as $listSandwich) {
                array_push($sandwiches, [
                    'sandwich' => $listSandwich,
                    'links' => ['self' => ['href' => $this->container['router']
                        ->pathFor('sandwichById', ['id' => $listSandwich->id])]]
                ]);
            }
            $data = [
                'count' => count($sandwiches),
                'sandwiches' => $sandwiches];
            return Writter::jsonSuccess($resp, $data, 200, 'collection');

        } catch (\Exception $exception) {
            return PhpError::error($req, $resp, 'Table empty');
        }
    }

    /** Méthode GET Sandwich by id
     * @param Request $req
     * @param Response $resp
     * @param Array $args
     * @return Response
     */
    public function getSandwich(Request $req, Response $resp, array $args): Response
    {
        try {
            $sandwich = [];
            $theSandwich = Sandwich::where('id', $args['id'])->firstOrFail();
            array_push($sandwich, [
                'id' => $theSandwich->id,
                'nom' => $theSandwich->nom,
                'description' => $theSandwich->description,
                'type_bread' => $theSandwich->type_pain,
                'price' => $theSandwich->prix,
                'img' => ['href' => 'img/' . $theSandwich->img]
            ]);
            $sandwichDetail = [
                'self' => [
                    'href' => $this->container['router']
                        ->pathFor('sandwichById', ['id' => $args['id']])
                ],
                'categories' => [
                    'href' => $this->container['router']
                        ->pathFor('categoriesOfASandwich', ['id' => $args['id']])
                ]
            ];
            $data = [
                'sandwich' => $sandwich,
                'links' => $sandwichDetail];
            return Writter::jsonSuccess($resp, $data);
        } catch (\Exception $exception) {
            return NotFound::error($req, $resp);
        }
    }

    /** Méthode GET Categories by Sandwich
     * @param Request $req
     * @param Response $resp
     * @param Array $args
     * @return Response
     */
    public function getCategoriesBySandwich(Request $req, Response $resp, array $args): Response
    {
        try {
            $sandwich = [];
            $theSandwich = Sandwich::where('id', $args['id'])->with('categories')->get();
            foreach ($theSandwich as $sand) {
                $categories = [];
                foreach ($sand->categories as $category) {
                    array_push($categories, [
                        'category' => [
                            'id' => $category->id,
                            'nom' => $category->nom
                        ],
                        'links' => [
                            'self' => [
                                'href' => $this->container['router']
                                    ->pathFor('categoryById', ['id' => $category->id])
                            ]
                        ]
                    ]);
                }
                array_push($sandwich, [
                    'id' => $sand->id,
                    'nom' => $sand->nom,
                    'description' => $sand->description,
                    'type_bread' => $sand->type_pain,
                    'price' => $sand->prix,
                    'img' => [
                        'href' => 'img/' . $sand->img
                    ],
                    'categories' => $categories
                ]);
            }
            $sandwichDetail = [
                'self' => [
                    'href' => $this->container['router']
                        ->pathFor('sandwichById', ['id' => $args['id']])
                ],
                'categories' => [
                    'href' => $this->container['router']
                        ->pathFor('categoriesOfASandwich', ['id' => $args['id']])
                ]
            ];
            $data = [
                'sandwich' => $sandwich,
                'links' => $sandwichDetail
            ];
            return Writter::jsonSuccess($resp, $data);

        } catch (\Exception $exception) {
            return PhpError::error($req, $resp, $exception->getMessage());
        }
    }

    /** Méthode GET Sandwich Filtered By Price
     * @param Request $req
     * @param Response $resp
     * @param Array $args
     * @return Response
     */
    public function getSandwichFilteredByPrice(Request $req, Response $resp, array $args): Response
    {
        try {
            $sandwiches = [];
            $priceSearch = $req->getQueryParam('maxprice');
            $listSandwiches = Sandwich::where('prix', '<=', $priceSearch)->get();
            foreach ($listSandwiches as $listSandwich) {
                array_push($sandwiches, [
                    'sandwich' => [
                        'id' => $listSandwich->id,
                        'nom' => $listSandwich->nom,
                        'prix' => $listSandwich->prix,
                    ],
                    'links' => ['self' => ['href' => $this->container['router']
                        ->pathFor('sandwichById', ['id' => $listSandwich->id])]]
                ]);
            }
            $data = [
                'count' => count($listSandwiches->toArray()),
                'sandwich' => $sandwiches,
                'links' => ['self' => ['href' => '/sandwiches?maxprice=' . $priceSearch]]
            ];
            return Writter::jsonSuccess($resp, $data, 200, 'collection');

        } catch (\Exception $exception) {
            return PhpError::error($req, $resp, $exception->getMessage());
        }
    }

    /** Méthode GET Sandwich Filtered By Bread
     * @param Request $req
     * @param Response $resp
     * @param Array $args
     * @return Response
     */
    public function getSandwichFilteredByBread(Request $req, Response $resp, array $args): Response
    {
        try {
            $sandwiches = [];
            $breadSearch = $req->getQueryParam('bread');
            $listSandwiches = Sandwich::where('type_pain', 'LIKE', "%$breadSearch%")->get();
            foreach ($listSandwiches as $listSandwich) {
                array_push($sandwiches, [
                    'sandwich' => [
                        'id' => $listSandwich->id,
                        'nom' => $listSandwich->nom,
                        'type_pain' => $listSandwich->type_pain,
                    ],
                    'links' => ['self' => ['href' => $this->container['router']
                        ->pathFor('sandwichById', ['id' => $listSandwich->id])]]
                ]);
            }
            $data = [
                'count' => count($listSandwiches->toArray()),
                'sandwiches' => $sandwiches,
                'links' => ['self' => ['href' => '/sandwiches?bread=' . $breadSearch]]
            ];
            return Writter::jsonSuccess($resp, $data, 200, 'collection');

        } catch (\Exception $exception) {
            return PhpError::error($req, $resp, $exception->getMessage());
        }
    }

    /** Méthode GET Sandwich Filtered By Bread And Price
     * @param Request $req
     * @param Response $resp
     * @param Array $args
     * @return Response
     */
    public function getSandwichFilteredByBreadAndPrice(Request $req, Response $resp, array $args): Response
    {
        try {
            $sandwiches = [];
            $breadSearch = $req->getQueryParam('bread');
            $priceSearch = $req->getQueryParam('maxprice');
            $listSandwiches = Sandwich::where([
                ['type_pain', 'LIKE', "%$breadSearch%"],
                ['prix', '<=', $priceSearch]
            ])->get();
            foreach ($listSandwiches as $listSandwich) {
                array_push($sandwiches, [
                    'sandwich' => [
                        'id' => $listSandwich->id,
                        'nom' => $listSandwich->nom,
                        'prix' => $listSandwich->prix,
                        'type_pain' => $listSandwich->type_pain
                    ],
                    'links' => ['self' => ['href' => $this->container['router']
                        ->pathFor('sandwichById', ['id' => $listSandwich->id])]]
                ]);
            }
            $data = [
                'count' => count($listSandwiches->toArray()),
                'sandwiches' => $sandwiches,
                'links' => ['self' => [
                    'href' => $this->container['router']
                            ->pathFor('allSandwiches')
                        . '?bread=' . $breadSearch
                        . "&maxprice=" . $priceSearch
                ]]
            ];
            return Writter::jsonSuccess($resp, $data, 200, 'collection');

        } catch (\Exception $exception) {
            return PhpError::error($req, $resp, $exception->getMessage());
        }
    }

    /** Méthode GET Sandwich Filtered By Fields
     * @param Request $req
     * @param Response $resp
     * @param Array $args
     * @return Response
     */
    public function getSandwichFilteredByFields(Request $req, Response $resp, array $args): Response
    {
        try {
            $fieldSearch = $req->getQueryParam('fields');
            $fields = explode(',', $fieldSearch);
            $sandwich = Sandwich::select($fields)->where('id', $args['id'])->firstOrFail();
            $sandwichDetail = [
                'self' => [
                    'href' => $this->container['router']
                        ->pathFor('sandwichById', ['id' => $args['id']])
                ],
                'categories' => [
                    'href' => $this->container['router']
                        ->pathFor('categoriesOfASandwich', ['id' => $args['id']])
                ]
            ];
            $data = [
                'sandwich' => $sandwich,
                'links' => $sandwichDetail
            ];
            return Writter::jsonSuccess($resp, $data);

        } catch (\Exception $exception) {

            return NotFound::error($req, $resp);
        }
    }

    /** Méthode GET Sandwiches Pages
     * @param Request $req
     * @param Response $resp
     * @param Array $args
     * @return Response
     */
    public function getSandwichesPages(Request $req, Response $resp, array $args): Response
    {
        try {
            $sandwiches = [];
            $page = $req->getQueryParam('page');
            $limit = $req->getQueryParam('limit');
            $offset = (--$page) * $limit;
            $count = Sandwich::all()->count();
            $lastPage = ceil($count / $limit);
            $listSandwiches = Sandwich::skip($offset)->take($limit)->get();
            foreach ($listSandwiches as $listSandwich) {
                array_push($sandwiches, [
                    'sandwich' => [
                        'id' => $listSandwich->id,
                        'nom' => $listSandwich->nom
                    ],
                    'links' => ['self' => ['href' => $this->container['router']
                        ->pathFor('sandwichById', ['id' => $listSandwich->id])]]
                ]);
            }
            $data = [
                'count' => count($listSandwiches->toArray()),
                'sandwiches' => $sandwiches,
                'links' => [
                    'prev' => ['href' => $this->container['router']
                            ->pathFor('allSandwiches')
                        . '?page=' . ($page)
                        . '&limit=' . $limit],
                    'next' => ['href' => $this->container['router']
                            ->pathFor('allSandwiches')
                        . '?page=' . ($page + 2)
                        . '&limit=' . $limit],
                    'first' => ['href' => $this->container['router']
                            ->pathFor('allSandwiches')
                        . '?page=1&limit=' . $limit],
                    'last' => ['href' => $this->container['router']
                            ->pathFor('allSandwiches')
                        . '?page=' . $lastPage
                        . '&limit=' . $limit]
                ]
            ];
            return Writter::jsonSuccess($resp, $data, 206, 'collection');

        } catch (\Exception $exception) {
            return PhpError::error($req, $resp, $exception->getMessage());
        }
    }

    /** Méthode DELETE Sandwich by id
     * @param Request $req
     * @param Response $resp
     * @param Array $args
     * @return Response
     */
    public function deleteSandwich(Request $req, Response $resp, array $args): Response
    {
        if (filter_var($args['id'], FILTER_VALIDATE_INT)) {
            $id = filter_var($args['id'], FILTER_VALIDATE_INT);
            try {
                $theSandwich = Sandwich::find($id);
                if (!empty($theSandwich)) {
                    try {
                        $theSandwich->delete();
                        return Writter::jsonSuccess($resp, [], 204, '');
                    } catch (\Exception $exception) {
                        return PhpError::error($req, $resp, $exception->getMessage());
                    }
                } else {
                    return NotFound::error($req, $resp);
                }

            } catch (\Exception $exception) {
                return PhpError::error($req, $resp, $exception->getMessage());
            }
        } else {
            return Writter::jsonError($resp, "Id is a integer", 400);
        }
    }

    /** Méthode POST Sandwich
     * @param Request $req
     * @param Response $resp
     * @param Array $args
     * @return Response
     */
    public function postSandwich(Request $req, Response $resp, array $args): Response
    {
        try {
            $param = $req->getParsedBody();

            $nom = filter_var($param['nom'], FILTER_SANITIZE_STRING);
            $description = filter_var($param['description'], FILTER_SANITIZE_STRING);
            $type_pain = filter_var($param['type_pain'], FILTER_SANITIZE_STRING);
            $img = filter_var($param['img'], FILTER_SANITIZE_STRING);
            $prix = filter_var($param['prix'], FILTER_SANITIZE_NUMBER_FLOAT);
            $categories = explode(',', $param['idcat']);

            $new_sandwich = new Sandwich();
            $new_sandwich->nom = $nom;
            $new_sandwich->description = $description;
            $new_sandwich->type_pain = $type_pain;
            $new_sandwich->img = $img;
            $new_sandwich->prix = $prix;
            $new_sandwich->save();

            foreach ($categories as $category) {
                $category = Categorie::find($category);
                $category->sandwiches()->attach($new_sandwich->id);
            }

            $sandwichDetail = [
                'self' => [
                    'href' => $this->container['router']
                        ->pathFor('sandwichById', ['id' => $new_sandwich->id])
                ],
                'categories' => [
                    'href' => $this->container['router']
                        ->pathFor('categoriesOfASandwich', ['id' => $new_sandwich->id])
                ]
            ];
            $data = [
                'sandwich' => $new_sandwich,
                'links' => $sandwichDetail];
            return Writter::jsonSuccess($resp, $data, 201);

        } catch (\Exception $exception) {
            return NotFound::error($req, $resp);
        }
    }

    /** Méthode PUT Sandwich
     * @param Request $req
     * @param Response $resp
     * @param Array $args
     * @return Response
     */
    public function updateSandiwch(Request $req, Response $resp, array $args): Response
    {
        $info = $req->getParsedBody();
        $id = $args['id'];
        try {
            $data = [];
            $sandwichOld = Sandwich::findOrFail($id);
            $sandwich = Sandwich::findOrFail($id);
            if (!empty($sandwich)) {
                if ($info['nom']) {
                    $sandwich->nom = $info['nom'];
                } else {
                    $sandwich->nom = $sandwichOld->nom;
                }
                if ($info['description']) {
                    $sandwich->description = $info['description'];
                } else {
                    $sandwich->description = $sandwichOld->description;
                }
                if ($info['type_pain']) {
                    $sandwich->type_pain = $info['type_pain'];
                } else {
                    $sandwich->type_pain = $sandwichOld->type_pain;
                }
                if ($info['img']) {
                    $sandwich->img = $info['img'];
                } else {
                    $sandwich->img = $sandwichOld->img;
                }
                if ($info['prix']) {
                    $sandwich->prix = $info['prix'];
                } else {
                    $sandwich->prix = $sandwichOld->prix;
                }
                $sandwich->save();
                array_push($data, ['changes' => [
                    'before' => $sandwichOld,
                    'after' => $sandwich
                ]]);
                return Writter::jsonSuccess($resp, $data, 200, 'collection');
            } else {
                $dataError = [
                    'type' => 'error',
                    'error' => 404,
                    'message' => "Sandwiche not found!"
                ];
                return $this->setResponse($resp, $dataError, array('Content-type' => 'application/json;charset=utf-8'), 404);
            }
        } catch (\Exception $e) {
            $dataError = [
                'type' => 'error',
                'error' => 404,
                'message' => "La ressource demandée n'existe pas : /sandwichs/" . $args['id']
            ];
            return PhpError::error($req, $resp, $e->getMessage());
        }
    }
}