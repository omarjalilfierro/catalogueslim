<?php

namespace src\api_catalogue;

use lbs\catalogue\controller\CatalogueController;
use lbs\catalogue\eloquent\Eloquent;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;
use Slim\Container;
use Slim\Exception\MethodNotAllowedException;
use Slim\Exception\NotFoundException;


require '../src/vendor/autoload.php';

$apiContainer = new Container(require_once __DIR__ . "/../src/conf/config.php");
$app = new App($apiContainer);

Eloquent::startEloquent($apiContainer->settings['dbconf']);

//ROUTES FOR CATEGORIES
$app->get('/categories[/]',
    function (Request $req, Response $resp, array $args) {
        return (new CatalogueController($this))->getCategories($req, $resp, $args);
    }
)->setName('categories');

$app->get('/categories/{id}[/]',
    function (Request $req, Response $resp, array $args) {
        return (new CatalogueController($this))->getCategory($req, $resp, $args);
    }
)->setName('categoryById');


$app->get('/categories/{id}/sandwiches[/]',
    function (Request $req, Response $resp, array $args) {
        return (new CatalogueController($this))->getSandwichesByCategory($req, $resp, $args);
    }
)->setName('sandwichesOfACategory');

//ROUTES FOR SANDWICHES
$app->get('/sandwiches[/]',
    function (Request $req, Response $resp, array $args) {
        if ($req->getQueryParam('bread') && $req->getQueryParam('maxprice')) {
            return (new CatalogueController($this))->getSandwichFilteredByBreadAndPrice($req, $resp, $args);
        } elseif ($req->getQueryParam('maxprice')) {
            return (new CatalogueController($this))->getSandwichFilteredByPrice($req, $resp, $args);
        } elseif ($req->getQueryParam('bread')) {
            return (new CatalogueController($this))->getSandwichFilteredByBread($req, $resp, $args);
        } elseif ($req->getQueryParam('page') && $req->getQueryParam('limit')) {
            return (new CatalogueController($this))->getSandwichesPages($req, $resp, $args);
        } else {
            return (new CatalogueController($this))->getSandwiches($req, $resp, $args);
        }
    }
)->setName('allSandwiches');


$app->get('/sandwiches/{id}[/]',
    function (Request $req, Response $resp, array $args) {
        if ($req->getQueryParam('fields')) {
            return (new CatalogueController($this))->getSandwichFilteredByFields($req, $resp, $args);
        } else {
            return (new CatalogueController($this))->getSandwich($req, $resp, $args);
        }
    }
)->setName('sandwichById');


$app->get('/sandwiches/{id}/categories[/]',
    function (Request $req, Response $resp, array $args) {
        return (new CatalogueController($this))->getCategoriesBySandwich($req, $resp, $args);
    }
)->setName('categoriesOfASandwich');


$app->delete('/sandwiches/{id}[/]',
    function (Request $req, Response $resp, array $args) {
        return (new CatalogueController($this))->deleteSandwich($req, $resp, $args);
    }
);

$app->post('/sandwiches[/]',
    function (Request $req, Response $resp, array $args) {
        return (new CatalogueController($this))->postSandwich($req, $resp, $args);
    }
);

$app->put('/sandwiches/{id}[/]',
    function (Request $request, Response $response, array $args) {
        return (new CatalogueController($this))->updateSandiwch($request, $response, $args);
    }
);

try {
    $app->run();
} catch (MethodNotAllowedException $e) {
    echo "MethodNotAllowedException error" . $e->getMessage();
} catch (NotFoundException $e) {
    echo "NotFoundException error" . $e->getMessage();
} catch (\Exception $e) {
    echo "Exception error" . $e->getMessage();
}
