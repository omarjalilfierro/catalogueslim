# LBS : Micro-services based application
## About : Buying sandwiches

## Services

* 1 service to access the catalog,
* 1 service takes the commands,
* 1 service follows the commands in the point of sale,
* 1 service catalog management

## Database

* 1 database catalog
* 1 database commands

# LBS : une application exemple à base de micro-services
## thématique : acheter des sandwichs

## Services

* 1 service pour l'accès au catalogue,
* 1 service de prise de commandes,
* 1 service de suivi des commandes point de vente,
* 1 service de gestion du catalogue

## Bases de données

* 1 base de données catalogue
* 1 base de données commandes

# URL :
###     127.0.0.1   api.catalogue-demo.local
###   - pour accéder à ce service : https://api.catalogue-demo.local:10443
###                          ou   :  http://api.catalogue-demo.local:10080

###     127.0.0.1   api.command-demo.local
###   - pour accéder à ce service : https://api.command-demo.local:20443
###                          ou   :  http://api.command-demo.local:20080

###     127.0.0.1   api.store-demo.local
###   - pour accéder à ce service : https://api.store-demo.local:30443
###                          ou   :  http://api.store-demo.local:30080

###     127.0.0.1   api.service.catalogue-demo.local
###   - pour accéder à ce service : https://api.service.catalogue-demo.local:40443
###                          ou   :  http://api.service.catalogue-demo.local:40080