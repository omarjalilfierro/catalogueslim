<?php
namespace src\api_serviceCatalogue;
session_start();

use lbs\serviceCatalogue\models\User;
use lbs\serviceCatalogue\controller\CatalogueController;
use lbs\serviceCatalogue\controller\SandwichController;
use lbs\serviceCatalogue\controller\UserController;
use lbs\serviceCatalogue\eloquent\Eloquent;
use lbs\serviceCatalogue\errors\MissingDataException;
use lbs\serviceCatalogue\response\Writter;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;
use Slim\Container;
use Slim\Exception\MethodNotAllowedException;
use Slim\Exception\NotFoundException;
use Illuminate\Database\Eloquent\ModelNotFoundException;


require '../src/vendor/autoload.php';

$apiContainer = new Container(require_once __DIR__ . "/../src/conf/config.php");
$app = new App($apiContainer);

Eloquent::startEloquent($apiContainer->settings['dbconf']);

$verifSession = function ($req, $resp, $next) {
    if(isset($_SESSION['userId'])){
        $result = User::find($_SESSION['userId'])->first();
        
        if(!empty($result)){
            return $next($req, $resp);
        }
        else{
            return $resp->withRedirect($this['router']
            ->pathFor('formLogin'), 301);
        }
    }
    else{
        return $resp->withRedirect($this['router']
        ->pathFor('formLogin'), 301);
    }
};

$app->get('/sandwiches/add[/]',
    function (Request $req, Response $resp, array $args) {
        return (new SandwichController($this))->getFormAddSandwich($req, $resp, $args);
    }
)->setName('formPostSandwiches')->add($verifSession);

$app->post('/sandwiches/add[/]',
    function (Request $req, Response $resp, array $args) {
        return (new SandwichController($this))->postSandwich($req, $resp, $args);
    }
)->add($verifSession);

$app->get('/categories[/]',
    function (Request $req, Response $resp, array $args) {
        return (new CatalogueController($this))->getCategories($req, $resp, $args);
    }
)->setName('allCategories')->add($verifSession);

$app->get('/sandwiches[/]',
    function (Request $req, Response $resp, array $args) {
        return (new SandwichController($this))->getSandwiches($req, $resp, $args);
    }
)->setName('allSandwiches')->add($verifSession);

$app->delete('/sandwiches/{id}[/]',
    function (Request $req, Response $resp, array $args) {
        return (new SandwichController($this))->deleteSandwich($req, $resp, $args);
    }
)->setName('deleteSand')->add($verifSession);

$app->put('/sandwiches/{id}[/]',
    function (Request $request, Response $response, array $args) {
        return (new SandwichController($this))->updateSandiwch($request, $response, $args);
    }
)->add($verifSession);

$app->get('[/]',
    function (Request $req, Response $resp, array $args) {
        return (new UserController($this))->formLogin($req, $resp, $args);
    }
)->setName('formLogin');

$app->post('[/]',
    function (Request $req, Response $resp, array $args) {
        return (new UserController($this))->login($req, $resp, $args);
    }
);

$app->get('/logout[/]',
    function (Request $req, Response $resp, array $args) {
        return (new UserController($this))->logout($req, $resp, $args);
    }
)->add($verifSession);

$app->run();