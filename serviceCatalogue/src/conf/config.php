<?php
/**
 * Created by PhpStorm.
 * User: jalil
 * Date: 12/13/18
 * Time: 10:11 AM
 */

use lbs\serviceCatalogue\response\Writter;

return [
    'settings' => ['displayErrorDetails' => true,
                   'dbconf' => __DIR__ . '/config.ini',
                   'tmpl_dir' => '../src/templates'],
    'view' => function( $c ) {
        return new \Slim\Views\Twig(
            $c['settings']['tmpl_dir'],
            ['debug' => true,
             'cache' => false //$c['settings']['tmpl_dir']
            ]
        );
    },
    //Slim named this error 'notFoundHandler','notAllowedHandler'
    //and it matches them automatically
    'notFoundHandler' => function ($container){
        return function ($rq, $rs){
            return \lbs\serviceCatalogue\errors\NotFound::error($rq, $rs);
        };
    },
    'notAllowedHandler' => function ($container){
        return function ($rq, $rs,$methods){
            return \lbs\serviceCatalogue\errors\NotAllowed::error($rq,$rs,$methods);
        };
    },
    'phpErrorHandler' => function ($container){
        return function ($rq, $rs,$error){
            return \lbs\serviceCatalogue\errors\PhpError::error($rq,$rs,$error);
        };
    },
    'errorHandler' => function( $container ) {
        return function ($rq, $rs, Exception $previousException, $message = '', $code = 400) {
            if (empty($message)) {
                $message = "Erreur lors de l'execution " . $previousException->getMessage();
            }
            return Writter::jsonerror($rs, $message, $code);
        };
    }

];
