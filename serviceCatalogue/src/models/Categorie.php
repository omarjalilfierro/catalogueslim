<?php

namespace lbs\serviceCatalogue\models;

class Categorie extends \Illuminate\Database\Eloquent\Model{

protected $table = 'categorie';
protected $primaryKey = 'id';
protected $timestamp = false;

    public function sandwiches(){
        return $this->belongsToMany('lbs\serviceCatalogue\models\Sandwich',
            'sand2cat',
            'cat_id',
            'sand_id');
    }


}
