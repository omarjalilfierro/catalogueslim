<?php

namespace lbs\serviceCatalogue\models;

class User extends \Illuminate\Database\Eloquent\Model{

    protected $table = 'user';
    protected $primaryKey = 'id';
    protected $timestamp = false;
    public $incrementing = false;
    protected $keyType = 'string';


}
