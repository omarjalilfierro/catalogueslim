<?php
/**
 * Created by PhpStorm.
 * User: jalil
 * Date: 12/18/18
 * Time: 3:47 PM
 */

namespace lbs\serviceCatalogue\models;
use Illuminate\Database\Eloquent\Model;

class Sandwich extends Model {

    protected $table = 'sandwich';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function categories(){
        return $this->belongsToMany('lbs\serviceCatalogue\models\Categorie',
            'sand2cat',
            'sand_id',
            'cat_id');
    }

}
