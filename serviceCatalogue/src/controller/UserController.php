<?php

namespace lbs\serviceCatalogue\controller;

use lbs\serviceCatalogue\models\User;
use lbs\serviceCatalogue\errors\MissingDataException;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Container;
use lbs\serviceCatalogue\response\Writter;
use \Slim\Views\Twig;
/* Errors */
use lbs\serviceCatalogue\errors\NotAllowed;
use lbs\serviceCatalogue\errors\NotFound;
use lbs\serviceCatalogue\errors\PhpError;


class UserController{

    protected $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /** Méthode formLogin
     * @param Request $req
     * @param Response $resp
     * @param Array $args
     * @return Response
      */
    public function formLogin(Request $req, Response $resp, array $args): Response{
        $missingToken = false;
        if(isset($_SESSION['userId'])){
            $result = User::find($_SESSION['userId'])->first();
            if(!empty($result)){
                return $resp->withRedirect($this->container['router']
                ->pathFor('allSandwiches'), 301);
            }
            else{
                $missingToken = true;
            }
        }
        else{
            $missingToken = true;
        }

        if($missingToken){
            $error['bool'] = false;
            $error['msg'] = "";
            
            $token = random_bytes(32);
            $token = bin2hex($token);
            $_SESSION['token'] = $token;

            return $this->container->view->render($resp, 'formLogin.html.twig', ['error' => $error, 'link' => $this->container['router']
            ->pathFor('formLogin'), 'token' => $token]);
        }
    }

    /** Méthode login
     * @param Request $req
     * @param Response $resp
     * @param Array $args
     * @return Response
      */
    public function login(Request $req, Response $resp, array $args): Response{
        $param = $req->getParsedBody();
        $error['bool'] = false;
        $error['msg'] = "";

        if(!empty($param['token'])){
            $token = filter_var($param['token'], FILTER_SANITIZE_STRING);
            if($token == $_SESSION['token']){
                if(!empty($param['identifiant'])){
                    $identifiant = filter_var($param['identifiant'], FILTER_SANITIZE_STRING);
                }
                else{
                    $error['bool'] = true;
                    $error['msg'] = "L'identifiant est manquant";
                }

                if(!empty($param['mdp'])){
                    $mdp = filter_var($param['mdp'], FILTER_SANITIZE_STRING);
                }
                else{
                    $error['bool'] = true;
                    $error['msg'] = "Le mot de passe est manquant";
                }

                if(!$error['bool']){
                    $user = User::where('identifiant','=', $identifiant)->first();

                    if(!empty($user)){
                        if(password_verify($mdp, $user->mdp)){
                            $_SESSION['userId'] = $user->id;
                            return $resp->withRedirect($this->container['router']->pathFor('allSandwiches'), 301);
                        }
                        else{
                            $error['bool'] = true;
                            $error['msg'] = "Identifiant ou mot de passe incorrect";
                        }
                    }
                    else{
                        $error['bool'] = true;
                        $error['msg'] = "Utilisateur inconnu";
                    }
                }

                if($error['bool']){
                    return $this->container->view->render($resp, 'formLogin.html.twig', ['error' => $error, 'link' => $this->container['router']
                    ->pathFor('formLogin'), 'token' => $token]);
                }
            }
            else{
                return $this->container->view->render($resp, 'error.html.twig',['msg' => "Le token est incorrect", 'link' => $this->container['router']
                ->pathFor('formLogin')]);
            }
        }
        else{
            return $this->formLogin($req, $resp, $args);
        }
    }

    /** Méthode logout
     * @param Request $req
     * @param Response $resp
     * @param Array $args
     * @return Response
      */
    public function logout(Request $req, Response $resp, array $args): Response{
        unset($_SESSION['userId']);
        return $resp->withRedirect($this->container['router']->pathFor('formLogin'), 301);
    }
}