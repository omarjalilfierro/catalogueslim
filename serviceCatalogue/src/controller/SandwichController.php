<?php

namespace lbs\serviceCatalogue\controller;

use lbs\serviceCatalogue\models\Categorie;
use lbs\serviceCatalogue\models\Sandwich;
use lbs\serviceCatalogue\errors\MissingDataException;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Container;
use lbs\serviceCatalogue\response\Writter;
use \Slim\Views\Twig;
/* Errors */
use lbs\serviceCatalogue\errors\NotAllowed;
use lbs\serviceCatalogue\errors\NotFound;
use lbs\serviceCatalogue\errors\PhpError;


class SandwichController{
    
    protected $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /** Méthode getFormAddSandwich
     * @param Request $req
     * @param Response $resp
     * @param Array $args
     * @return Response
      */
    public function getFormAddSandwich(Request $req, Response $resp, array $args): Response{
        $error['bool'] = false;
        $error['msg'] = "";

        $categories = Categorie::all();

        /* Token CSRF */
        $token = random_bytes(32);
        $token = bin2hex($token);
        $_SESSION['token'] = $token;

        return $this->container->view->render($resp, 'formPostSandwich.html.twig', ['error' => $error, 'link' => $this->container['router']
        ->pathFor('formPostSandwiches'), 'token' => $token, 'categories' => $categories]);
    }

    /** Méthode postSandwich
     * @param Request $req
     * @param Response $resp
     * @param Array $args
     * @return Response
      */
    public function postSandwich(Request $req, Response $resp, array $args): Response{
        $param = $req->getParsedBody();
        $error['bool'] = false;
        $error['msg'] = "";
        if(!empty($param['token'])){
            $token = $param['token'];
            if($token == $_SESSION['token']){
                $img = NULL;
                if(!empty($param['nom'])){
                    $nom = filter_var($param['nom'], FILTER_SANITIZE_STRING);
                }
                else{
                    $error['bool'] = true;
                    $error['msg'] = "Le nom est manquant";
                }

                if(!empty($param['description'])){
                    $description = filter_var($param['description'], FILTER_SANITIZE_STRING);
                }
                else{
                    $error['bool'] = true;
                    $error['msg'] = "La description est manquante";
                }

                if(!empty($param['type_pain'])){
                    $type_pain = filter_var($param['type_pain'], FILTER_SANITIZE_STRING);
                }
                else{
                    $error['bool'] = true;
                    $error['msg'] = "Le type de pain est manquant";
                }

                if(!empty($_FILES['img']['name'])){
                    $img = filter_var($_FILES['img']['name'], FILTER_SANITIZE_STRING);

                    $uploaddir = '../src/templates/assets/';
                    $uploadfile = $uploaddir . basename($_FILES['img']['name']);

                    if (move_uploaded_file($_FILES['img']['tmp_name'], $uploadfile)) {

                    }
                    else {
                        $error['bool'] = true;
                        $error['msg'] = "Impossible de télécharger le fichier";
                    }
                }

                if(!empty($param['prix'])){
                    $prix = filter_var($param['prix'], FILTER_SANITIZE_NUMBER_FLOAT);
                }
                else{
                    $error['bool'] = true;
                    $error['msg'] = "Le prix est manquant";
                }

                if(!empty($param['category'])){
                    $category = filter_var($param['category'], FILTER_SANITIZE_NUMBER_FLOAT);
                }
                else{
                    $error['bool'] = true;
                    $error['msg'] = "La categorie est manquant";
                }

                if(!$error['bool']){
                    try {
                        $new_sandwich = new Sandwich();
                        $new_sandwich->nom = $nom;
                        $new_sandwich->description =  $description;
                        $new_sandwich->type_pain =  $type_pain;
                        $new_sandwich->img =  $img;
                        $new_sandwich->prix = $prix;
                        $new_sandwich->save();
                        
                        $new_sandwich->categories()->attach($category);
                        $new_sandwich->save();
            
                        $msg = "Le sandwich $new_sandwich->nom est ajouté !";
                        return $this->container->view->render($resp, 'succes.html.twig', ['msg' => $msg, 'link' => $this->container['router']
                        ->pathFor('allSandwiches')]);
            
                    } catch (\Exception $exception) {
                        return PhpError::error($req, $resp, $exception);
                    }
                }
                else{
                    $categories = Categorie::all();

                    return $this->container->view->render($resp, 'formPostSandwich.html.twig', ['error' => $error, 'link' => $this->container['router']
                    ->pathFor('formPostSandwiches'), 'token' => $token, 'categories' => $categories]);
                }
            }
            else{
                return $this->container->view->render($resp, 'error.html.twig',['msg' => "Le token est incorrect", 'link' => $this->container['router']
                ->pathFor('formPostSandwiches')]);
            }
        }
        else{
            return $this->getFormAddSandwich($req, $resp, $args);
        }
    }

    /** Méthode getSandwiches
     * @param Request $req
     * @param Response $resp
     * @param Array $args
     * @return Response
      */
    public function getSandwiches(Request $req, Response $resp, array $args): Response
    {
        try {
            $listSandwiches = Sandwich::with('categories')->get();

            return $this->container->view->render($resp, 'sandwiches_list.html.twig', 
                [
                    'sandwiches' => $listSandwiches,
                ] 
            );

        } catch (\Exception $exception) {
            return PhpError::error($req,$resp, $exception->getMessage());
        }
    }


    /** Méthode getSandwichesByCategory
     * @param Request $req
     * @param Response $resp
     * @param Array $args
     * @return Response
      */
    public function getSandwichesByCategory(Request $req, Response $resp, array $args): Response
    {
            $category = [];
            $cat = Categorie::where('id', $args['id'])->with('sandwiches')->first();
            $sandwiches = $cat->sandwiches;
            
            return $this->container->view->render($resp, 'categ_sand.html.twig', 
                [
                    'category' => $cat,
                    'sandwiches' => $sandwiches,
                ] 
            );
            
            return Writter::htmlSuccess($resp, $html);

    }

    /** Méthode deleteSandwich
     * @param Request $req
     * @param Response $resp
     * @param Array $args
     * @return Response
      */
    public function deleteSandwich(Request $req, Response $resp, array $args): Response
    {
        // if(filter_var($args['id'], FILTER_VALIDATE_INT)){
            $id = filter_var($args['id'], FILTER_VALIDATE_INT);
            try {
                $theSandwich = Sandwich::find($id);
                
                if(!empty($theSandwich)){
                    try{
                        $theSandwich->delete();
                        return Writter::jsonSuccess($resp, [], 204, '');
                    } catch (\Exception $exception) {
                        return PhpError::error($req, $resp, $exception->getMessage());
                    }
                }
                else{
                    return NotFound::error($req, $resp);
                }
    
            } catch(\Exception $exception){
                return PhpError::error($req, $resp, $exception->getMessage());
            }
        // }
        // else{
        //     return Writter::jsonError($resp, "Id is a integer", 400);
        // }
    }

    /** Méthode PUT Sandwich
     * @param Request $req
     * @param Response $resp
     * @param Array $args
     * @return Response
     */
    public function updateSandiwch(Request $req, Response $resp, array $args): Response
    {
        $info = $req->getParsedBody();
        $id = $args['id'];
        try {
            $data = [];
            $sandwichOld = Sandwich::findOrFail($id);
            $sandwich = Sandwich::findOrFail($id);
            if (!empty($sandwich)) {
                if ($info['nom']) {
                    $sandwich->nom = $info['nom'];
                } else {
                    $sandwich->nom = $sandwichOld->nom;
                }
                if ($info['description']) {
                    $sandwich->description = $info['description'];
                } else {
                    $sandwich->description = $sandwichOld->description;
                }
                if ($info['type_pain']) {
                    $sandwich->type_pain = $info['type_pain'];
                } else {
                    $sandwich->type_pain = $sandwichOld->type_pain;
                }
                if ($info['img']) {
                    $sandwich->img = $info['img'];
                } else {
                    $sandwich->img = $sandwichOld->img;
                }
                if ($info['prix']) {
                    $sandwich->prix = $info['prix'];
                } else {
                    $sandwich->prix = $sandwichOld->prix;
                }
                $sandwich->save();
                array_push($data, ['changes' => [
                    'before' => $sandwichOld,
                    'after' => $sandwich
                ]]);
                return Writter::jsonSuccess($resp, $data, 200, 'collection');
            } else {
                $dataError = [
                    'type' => 'error',
                    'error' => 404,
                    'message' => "Sandwiche not found!"
                ];
                return $this->setResponse($resp, $dataError, array('Content-type' => 'application/json;charset=utf-8'), 404);
            }
        } catch (\Exception $e) {
            $dataError = [
                'type' => 'error',
                'error' => 404,
                'message' => "La ressource demandée n'existe pas : /sandwichs/" . $args['id']
            ];
            return PhpError::error($req, $resp, $e->getMessage());
        }
    }
}