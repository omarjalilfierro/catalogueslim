<?php
/**
 * Created by PhpStorm.
 * User: jalil
 * Date: 12/13/18
 * Time: 9:32 AM
 */

namespace lbs\serviceCatalogue\controller;

use lbs\serviceCatalogue\models\Categorie;
use lbs\serviceCatalogue\errors\MissingDataException;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Container;
use lbs\serviceCatalogue\response\Writter;
/* Errors */
use lbs\serviceCatalogue\errors\NotAllowed;
use lbs\serviceCatalogue\errors\NotFound;
use lbs\serviceCatalogue\errors\PhpError;


class CatalogueController{
    
    protected $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }


    /** Méthode getCategories
     * @param Request $req
     * @param Response $resp
     * @param Array $args
     * @return Response
      */
    public function getCategories(Request $req, Response $resp, array $args): Response
    {
        try {
            $listCategories = Categorie::with('sandwiches')->get();

            return $this->container->view->render($resp, 'categ_sand.html.twig', 
                [
                    'categories' => $listCategories,
                ] 
            );

        } catch (\Exception $exception) {
            return PhpError::error($req,$resp, $exception->getMessage());
        }
    }
    
}