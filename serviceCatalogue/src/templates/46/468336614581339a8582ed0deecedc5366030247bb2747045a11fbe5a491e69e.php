<?php

/* succes.html.twig */
class __TwigTemplate_7df89f75afff6adfc0aedbe1ff774b6127bd7ca72fbcfc96ca443ab89aa4f84e extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<h1>Succes :</h1>
<p>";
        // line 2
        echo twig_escape_filter($this->env, ($context["msg"] ?? null), "html", null, true);
        echo "</p>";
    }

    public function getTemplateName()
    {
        return "succes.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  26 => 2,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<h1>Succes :</h1>
<p>{{ msg }}</p>", "succes.html.twig", "/var/www/src/templates/succes.html.twig");
    }
}
