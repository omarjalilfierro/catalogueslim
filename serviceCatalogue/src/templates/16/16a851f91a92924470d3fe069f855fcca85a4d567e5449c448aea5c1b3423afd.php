<?php

/* formPostSandwich.html.twig */
class __TwigTemplate_70e38167e39f5a09dc77511a65d81b25b88004e63592b627188f9a8d2a0963c6 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<h1>Ajouter un sandwich</h1>
";
        // line 2
        if ((($__internal_7cd7461123377b8c9c1b6a01f46c7bbd94bd12e59266005df5e93029ddbc0ec5 = ($context["error"] ?? null)) && is_array($__internal_7cd7461123377b8c9c1b6a01f46c7bbd94bd12e59266005df5e93029ddbc0ec5) || $__internal_7cd7461123377b8c9c1b6a01f46c7bbd94bd12e59266005df5e93029ddbc0ec5 instanceof ArrayAccess ? ($__internal_7cd7461123377b8c9c1b6a01f46c7bbd94bd12e59266005df5e93029ddbc0ec5["bool"] ?? null) : null)) {
            // line 3
            echo "    ";
            echo twig_escape_filter($this->env, (($__internal_3e28b7f596c58d7729642bcf2acc6efc894803703bf5fa7e74cd8d2aa1f8c68a = ($context["error"] ?? null)) && is_array($__internal_3e28b7f596c58d7729642bcf2acc6efc894803703bf5fa7e74cd8d2aa1f8c68a) || $__internal_3e28b7f596c58d7729642bcf2acc6efc894803703bf5fa7e74cd8d2aa1f8c68a instanceof ArrayAccess ? ($__internal_3e28b7f596c58d7729642bcf2acc6efc894803703bf5fa7e74cd8d2aa1f8c68a["msg"] ?? null) : null), "html", null, true);
            echo "
";
        }
        // line 5
        echo "<form method=\"post\" action=\"";
        echo twig_escape_filter($this->env, ($context["link"] ?? null), "html", null, true);
        echo "\" enctype=\"multipart/form-data\">
    <input type=\"hidden\" name=\"token\" value=\"";
        // line 6
        echo twig_escape_filter($this->env, ($context["token"] ?? null), "html", null, true);
        echo "\" />
    <label>Nom :</label><br />
    <input type=\"text\" name=\"nom\" required/><br />
    <label>Description :</label><br />
    <input type=\"text\" name=\"description\" required/><br />
    <label>Type pain :</label><br />
    <input type=\"text\" name=\"type_pain\" required/><br />
    <label>Image</label><br />
    <input type=\"file\" name=\"img\"><br />
    <label>Prix :</label><br />
    <input type=\"text\" name=\"prix\" required/><br />
    <label>Categorie :</label><br />
    <select name=\"category\" required>
        ";
        // line 19
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["categories"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
            // line 20
            echo "            <option value=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "id", []), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "nom", []), "html", null, true);
            echo "</option>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 22
        echo "    </select><br />
    <input type=\"submit\" name=\"btSubmit\" value=\"Ajouter\"/>
</form>";
    }

    public function getTemplateName()
    {
        return "formPostSandwich.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  70 => 22,  59 => 20,  55 => 19,  39 => 6,  34 => 5,  28 => 3,  26 => 2,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<h1>Ajouter un sandwich</h1>
{% if error['bool'] %}
    {{ error['msg'] }}
{% endif %}
<form method=\"post\" action=\"{{ link }}\" enctype=\"multipart/form-data\">
    <input type=\"hidden\" name=\"token\" value=\"{{ token }}\" />
    <label>Nom :</label><br />
    <input type=\"text\" name=\"nom\" required/><br />
    <label>Description :</label><br />
    <input type=\"text\" name=\"description\" required/><br />
    <label>Type pain :</label><br />
    <input type=\"text\" name=\"type_pain\" required/><br />
    <label>Image</label><br />
    <input type=\"file\" name=\"img\"><br />
    <label>Prix :</label><br />
    <input type=\"text\" name=\"prix\" required/><br />
    <label>Categorie :</label><br />
    <select name=\"category\" required>
        {% for category in categories %}
            <option value=\"{{ category.id }}\">{{ category.nom }}</option>
        {% endfor %}
    </select><br />
    <input type=\"submit\" name=\"btSubmit\" value=\"Ajouter\"/>
</form>", "formPostSandwich.html.twig", "/var/www/src/templates/formPostSandwich.html.twig");
    }
}
