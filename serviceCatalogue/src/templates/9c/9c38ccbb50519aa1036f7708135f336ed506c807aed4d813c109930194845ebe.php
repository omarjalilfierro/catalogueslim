<?php

/* error.html.twig */
class __TwigTemplate_e20efe1f295ab2e25e4b34867149cbb7cc8c452dd5e13ecddd6f1c8ea2b91936 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<h1>Erreur :</h1>
<p>";
        // line 2
        echo twig_escape_filter($this->env, ($context["msg"] ?? null), "html", null, true);
        echo "</p>";
    }

    public function getTemplateName()
    {
        return "error.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  26 => 2,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<h1>Erreur :</h1>
<p>{{ msg }}</p>", "error.html.twig", "/var/www/src/templates/error.html.twig");
    }
}
